import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    titles = ["Mr.", "Mrs.", "Miss"]
    results = []
    for prefix in titles:
        title_df = df[df['Name'].str.contains(prefix)]
        missing_values = title_df['Age'].isnull().sum()
        median_age = round(title_df['Age'].median())
        results.append((prefix, missing_values, median_age))
    return results
print(get_filled())
